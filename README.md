# hc2i_bricoma_sale_mobile_app

Bricoma seles mobile app 

**E-Commerce directory Management System **


- Google Maps for update or select your shipping address (15h) 
- Entities & Models (20h). 
- Clean & organized Dart Language code using MVC Pattern (20h)
- Easy to restyle and theming by on your branding sites / Awesome animations are ready to use: Parallax Animations, Sliding & Swiping animations (30h)
- Working really well on both iOS and Android with support with 60 frames per second (fps) (15h + 38h dep).
- E-Payment, credit cards integration (10h).
- Cash on delivery and payment on pickup methods (10h)
- Support RTL languages (Arabic, Farisi…) (15h)
- Push notification using FCM (Socket) (22h)
- Login / Register / Forgot password (15h)
- Home / Maps Explorer / Products Listing / Product Details (20h)
- Cart / Checkout (20h)
- User Profile / User Orders / User Favorites Products (20h)
- Help & Supports (6h)
- Product and Store Reviews / Tracking Orders (10h)

**35 days**
